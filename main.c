/**
 * Progetto di Algoritmi e Informatica Teorica.
 * Studente: Harpal Singh
 * Matricola: ------
 * Anno: 2019
 */


/**
 * Functions to implement
 * Adds
 * TODO: addent <id_ent>
 * Adds a new entity that will be monitered, if it is already present don't do anything
 * TODO: delent <id_ent>
 * Deletes a given entity, if it is not already present don't do anything
 * TODO: addrel <id_orig> <id_dest> <id_rel>
 * Creates a new relation with the given name from origin entity to the destination entity.
 * If the relation is already present don't do anything. If any of the given entities are not present
 * don't do anything.
 * TODO: delrel <id_orig> <id_dest> <id_rel>
 * Deletes the given reletion between entities if present.
 * TODO: report
 * Prints the report of the entities and relations that are being monitored.
 * TODO: end
 * End the program
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAX_LENGHT_INPUT 255

struct entity {
    char *name;
    int childrenCount;
    struct entity *next;
    struct entity *children;
};


typedef struct entity *ENTITY;

ENTITY isEntityPresent(ENTITY root, char *entityName);

ENTITY insertEntityWithCount(ENTITY *head, char *entityName, int count);

ENTITY insertEntity(ENTITY *head, char *entityName);

void removeEntity(ENTITY *head, char *entityName);

void removeFromRelations(ENTITY *head, char *entityName);

void addRel(ENTITY entitiesList, char *orig, char *dest, char *rel);

void delRel(ENTITY entitiesList, char *orig, char *dest, char *rel);

void removeList(ENTITY *root);

void printRelations(ENTITY root);

ENTITY MergeEntities(ENTITY a, ENTITY b);

void SplitListInTwo(ENTITY source,
                    ENTITY *frontRef, ENTITY *backRef);


int main() {
    char *orig;
    char *dest;
    char *rel;
    size_t bufsize = MAX_LENGHT_INPUT;
    const char s[4] = " \"\n";
    char *buffer = (char *) malloc(bufsize * sizeof(char));
    if (buffer == NULL) {
        perror("Unable to allocate buffer");
        exit(1);
    }
    getline(&buffer, &bufsize, stdin);
    char *input = strtok(buffer, s);
    ENTITY entities = NULL;
    while (strcmp(input, "end") != 0) {
        if (strcmp(input, "addent") == 0) {
            input = strtok(NULL, s);
            if (input != NULL) {
                insertEntity(&entities, input);
            }
        } else if (strcmp(input, "delent") == 0) {
            input = strtok(NULL, s);
            if (input != NULL) {
                removeEntity(&entities, input);
                removeFromRelations(&entities, input);
            }
        } else if (strcmp(input, "addrel") == 0) {
            input = strtok(NULL, s);
            if (input != NULL) {
                orig = input;
                input = strtok(NULL, s);
                if (input != NULL) {
                    dest = input;
                    input = strtok(NULL, s);
                    if (input != NULL) {
                        rel = input;
                        addRel(entities, orig, dest, rel);
                    }
                }
            }
        } else if (strcmp(input, "delrel") == 0) {
            input = strtok(NULL, s);
            if (input != NULL) {
                orig = input;
                input = strtok(NULL, s);
                if (input != NULL) {
                    dest = input;
                    input = strtok(NULL, s);
                    if (input != NULL) {
                        rel = input;
                        delRel(entities, orig, dest, rel);
                    }
                }
            }
        } else if (strcmp(input, "report") == 0) {
            printRelations(entities);
        } else {
            printf("I am  outside of the normal!");
        }
        getline(&buffer, &bufsize, stdin);
        input = strtok(buffer, s);
    }
    return 0;
}

/*
 * Merge function used to merge linked lists
 */
void MergeSort(ENTITY *headRef) {
    ENTITY head = *headRef;
    ENTITY a;
    ENTITY b;

    if ((head == NULL) || (head->next == NULL)) {
        return;
    }

    SplitListInTwo(head, &a, &b);

    MergeSort(&a);
    MergeSort(&b);

    *headRef = MergeEntities(a, b);
}

/*
 * Merge two linked lists already ordered
 */
ENTITY MergeEntities(ENTITY a, ENTITY b) {
    ENTITY result = NULL;

    if (a == NULL)
        return (b);
    else if (b == NULL)
        return (a);

    if (strcmp(a->name, b->name) <= 0) {
        result = a;
        result->next = MergeEntities(a->next, b);
    } else {
        result = b;
        result->next = MergeEntities(a, b->next);
    }
    return (result);
}

/*
 * Split the linked list by fast/slow pointer strategy,
 * basically we iterate over the linked twice as fast to get the middle entity,
 * if it is odd the next entity is placed in the remaining item is placed in backRef
 */
void SplitListInTwo(ENTITY source, ENTITY *frontRef, ENTITY *backRef) {
    ENTITY fast;
    ENTITY slow;
    slow = source;
    fast = source->next;

    while (fast != NULL) {
        fast = fast->next;
        if (fast != NULL) {
            slow = slow->next;
            fast = fast->next;
        }
    }

    *frontRef = source;
    *backRef = slow->next;
    slow->next = NULL;
}

/*
 * Checks if the entityName is already present in the list
 */
ENTITY isEntityPresent(ENTITY root, char *entityName) {
    ENTITY temp = root;
    while (temp != NULL) {
        if (strcmp(temp->name, entityName) == 0) return temp;
        temp = temp->next;
    }
    return NULL;
}

/*
 * Inserts a new entity if not present and sets also the count parameter,
 * return the entity inserted or that is already present
 */
ENTITY insertEntityWithCount(ENTITY *head, char *entityName, int count) {
    ENTITY t;
    ENTITY presentEntity = isEntityPresent(*head, entityName);
    if (!presentEntity) {
        t = (ENTITY) malloc(sizeof(struct entity));
        char *entityNameHolder = (char *) malloc(strlen(entityName) * sizeof(char));
        if (*head == NULL) {
            *head = t;
            (*head)->name = entityNameHolder;
            strcpy((*head)->name, entityName);
            (*head)->childrenCount = count;
            (*head)->children = NULL;
            (*head)->next = NULL;
            return t;
        } else {
            t->name = entityNameHolder;
            strcpy(t->name, entityName);
            t->next = (*head);
            t->children = NULL;
            t->childrenCount = count;
            (*head) = t;
        }
        return t;
    } else {
        presentEntity->childrenCount = count;
        return presentEntity;
    }
}

/*
 * Inserts new entity if not present without setting the counting parameter
 */
ENTITY insertEntity(ENTITY *head, char *entityName) {
    return insertEntityWithCount(head, entityName, 0);
}

/*
 * Removes an entity if present, and remove all the childrens' lists if present
 * At last free the memory
 */
void removeEntity(ENTITY *head, char *entityName) {
    ENTITY temp = *head, prev;

    if (temp != NULL && strcmp(temp->name, entityName) == 0) {
        *head = temp->next;
        while (temp->children != NULL) {
            removeEntity(&(temp->children), temp->children->name);
        }
        free(temp->name);
        free(temp);
        return;
    }

    while (temp != NULL && strcmp(temp->name, entityName) != 0) {
        prev = temp;
        temp = temp->next;
        if (temp == NULL) return;
        if (strcmp(temp->name, entityName) == 0) {
            prev->next = temp->next;
            while (temp->children != NULL) {
                removeEntity(&(temp->children), temp->children->name);
            }
            free(temp->name);
            free(temp);
            return;
        }
    }
}

/*
 * Removes leafs of relations with name entityName
 */
void removeFromRelations(ENTITY *head, char *entityName) {
    ENTITY temp = *head;

    while (temp != NULL) {
        ENTITY relation = temp->children;
        while (relation != NULL) {
            removeEntity(&(relation->children), entityName);
            ENTITY tempNextRel = relation->next;
            if (relation->children == NULL) {
                removeEntity(&(temp->children), relation->name);
            }
            relation = tempNextRel;
        }
        temp = temp->next;
    }

}

/*
 * Adds a new relation if not already present
 */
void addRel(ENTITY entitiesList, char *orig, char *dest, char *rel) {
    ENTITY destEnt = isEntityPresent(entitiesList, dest);
    ENTITY origEnt = isEntityPresent(entitiesList, orig);
    if (destEnt == NULL || origEnt == NULL) {
        return;
    } else {
        ENTITY relEntity = insertEntity(&(destEnt->children), rel);
        insertEntity(&(relEntity->children), orig);
    }
}

/*
 * Deletes a relation if present
 */
void delRel(ENTITY entitiesList, char *orig, char *dest, char *rel) {
    ENTITY destEnt = isEntityPresent(entitiesList, dest);
    ENTITY origEnt = isEntityPresent(entitiesList, orig);
    if (destEnt == NULL || origEnt == NULL) {
        return;
    } else {
        ENTITY relEntity = isEntityPresent(destEnt->children, rel);
        if (relEntity != NULL) {
            removeEntity(&(relEntity->children), orig);
            if (relEntity->children == NULL) {
                removeEntity(&(destEnt->children), rel);
            }
        }
    }
}

/*
 * removes a list from memory and deletes all
 */
void removeList(ENTITY *root) {
    ENTITY temp = *root;
    while (temp != NULL) {
        ENTITY z = temp->next;
        free(temp->name);
        free(temp);
        temp = z;
    }
    *root = NULL;
}


/*
 * Function used to print the report of the relations
 * It basically creates a linked list of relations
 * All relations contains the number of children it has
 */
void printRelations(ENTITY root) {
    ENTITY relation_list = NULL;
    ENTITY destinationEntities = root;

    while (destinationEntities != NULL) {
        // Gets the relations list and iterate over it
        ENTITY relation = destinationEntities->children;
        while (relation != NULL) {
            // inserts the relation or finds if already inserted
            ENTITY insertedEntity = insertEntity(&relation_list, relation->name);
            // counts the children inside the original relation
            int count = 0;
            ENTITY orig = relation->children;
            while (orig != NULL) {
                count++;
                orig = orig->next;
            }
            ENTITY destEntities = insertedEntity->children;
            if (destEntities == NULL) {
                insertEntityWithCount(&(insertedEntity->children), destinationEntities->name, count);
            } else {
                if (destEntities->childrenCount < count) {
                    removeList(&(insertedEntity->children));
                    insertEntityWithCount(&(insertedEntity->children), destinationEntities->name, count);
                } else if (destEntities->childrenCount == count) {
                    insertEntityWithCount(&(insertedEntity->children), destinationEntities->name, count);
                }
            }
            relation = relation->next;
        }
        destinationEntities = destinationEntities->next;
    }
    destinationEntities = relation_list;
    MergeSort(&destinationEntities);
    if (relation_list == NULL) {
        printf("none\n");
        return;
    }
    while (destinationEntities != NULL) {
        printf("\"%s\"", destinationEntities->name);
        ENTITY dest = destinationEntities->children;
        MergeSort(&dest);
        int count = 0;
        while (dest != NULL) {
            printf(" \"%s\"", dest->name);
            count = dest->childrenCount;
            dest = dest->next;
        }
        printf(" %d;", count);
        destinationEntities = destinationEntities->next;
        if (destinationEntities != NULL) {
            printf(" ");
        }
    }

    printf("\n");
    while (relation_list != NULL) {
        removeEntity(&relation_list, relation_list->name);
    }
}